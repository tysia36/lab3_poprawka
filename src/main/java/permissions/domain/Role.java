/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package permissions.domain;

import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author j.pluzinska
 */
public class Role {
	
	private int id;
    private String role;
    private List <Permission> listA = new ArrayList<Permission>();
    
    public Role() {
 	}
 	
 	public Role(String role) {
 		super();
 		this.role=role;
 	}
     
     public int getId() {
 		return id;
 	}

 	public void setId(int id) {
 		this.id = id;
 	}
	public String getRole() {
		return this.role;
                
	}

	public void setRole(String role) {
		this.role = role;
	}

	public List <Permission> getListA() {
		return listA;
	}

	public void setListA(List <Permission> listA) {
		this.listA = listA;
	}

}