/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package permissions.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author j.pluzinska
 */
public class Person {
	
	private int id;
    private String name;
    private String surname;
    private List <Address> listA = new ArrayList<Address>();
    private List <Integer> listTel = new ArrayList<Integer>();
    private List <Role> listRole = new ArrayList<Role>();
    private User u = new User();
    
    public Person() {
   	}
   	
   	public Person(String name, String surname) {
   		super();
   		this.name = name;
   		this.surname = surname;
   	}
       
       public int getId() {
   		return id;
   	}

   	public void setId(int id) {
   		this.id = id;
   	}
    
    public User getU() {
		return u;
	}

	public void setU(User u) {
		this.u = u;
	}

	public List <Address> getListA() {
		return listA;
	}

	public List <Integer> getListTel() {
		return listTel;
	}

	public List <Role> getListRole() {
		return listRole;
	}

	public String getName() 
    {
	return this.name;
    }

    public void setName(String name) 
    {
	this.name = name;
    }
    public String getSurname() 
    {
	return this.surname;
    }

    public void setSurname(String surname) 
    {
	this.surname = surname;
    }

	public void setListA(List <Address> listA) {
		this.listA = listA;
	}

	public void setListTel(List <Integer> listTel) {
		this.listTel = listTel;
	}

	public void setListRole(List <Role> listRole) {
		this.listRole = listRole;
	}
        
    
}