package permissions;
import java.sql.Connection;

import java.sql.*;
import java.util.*;



import permissions.db.repos.*; 
import permissions.domain.*;

public class Main {

		public static void main(String[] args) {
			
			String url = "jdbc:hsqldb:hsql://localhost/workdb";
			
			User one = new User();
			one.setLogin("admin");
			one.setPassword("P@$$w0rd");
			
			Address two = new Address();
			two.setCity("Town");
			two.setStreet("Street");
			two.setFlatNumber(1);
			two.setHouseNumber(2);
			
			Permission everything = new Permission();
			everything.setPermission("everything");
			
			Permission nothing = new Permission();
			nothing.setPermission("nothing");
			
			Role some = new Role();
			some.setRole("admin");
			Role none = new Role();
			none.setRole("noobie");
			List <Permission> l = new ArrayList<Permission>();
			l.add(everything);
			l.add(nothing);
			some.setListA(l);
			
			Person someone = new Person();
			someone.setName("I");
			someone.setSurname("Am");
			someone.setU(one);
			List <Address> l2 = new ArrayList<Address>();
			l2.add(two);
			someone.setListA(l2);
			List <Integer> l3 = new ArrayList<Integer>();
			l3.add(123456789);
			l3.add(987654321);
			someone.setListTel(l3);
			List <Role> l4 = new ArrayList<Role>();
			l4.add(some);
			l4.add(none);
			someone.setListRole(l4);
			
			try {
				
				Connection connection = DriverManager.getConnection(url);
				
				HsqlUserRepository u = new HsqlUserRepository(connection);
				u.add(one);
				one.setLogin("admino");
				u.modify(one);
				u.remove(one);
				
				HsqlAddressRepository a = new HsqlAddressRepository(connection);
				a.add(two);
				two.setHouseNumber(12);
				a.modify(two);
				a.remove(two);
				
				HsqlPermissionRepository p = new HsqlPermissionRepository(connection);
				p.add(everything);
				everything.setPermission("everything else");
				p.modify(everything);
				p.remove(everything);
				
				HsqlRoleRepository r = new HsqlRoleRepository(connection);
				r.add(some);
				some.setRole("user");
				r.modify(some);
				r.remove(some);
				
				HsqlPersonRepository per = new HsqlPersonRepository(connection);
				per.add(someone);
				someone.setName("You");
				someone.setSurname("Are");
				per.modify(someone);
				per.remove(someone);
				
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			System.out.println("koniec");

	}

}
